package org.springframework.samples.petclinic.model;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

import javax.transaction.Transactional;

import org.junit.jupiter.api.Test;

public class PetTest {

	@Test
	@Transactional
    void testHasVisit() throws Exception {
    	Pet pet = new Pet();
		Visit visit = new Visit();
		visit.setDescription("visit test");
		assertNull(pet.getVisit("visit test"));
		assertNull(pet.getVisit("Visit Test"));
		pet.addVisit(visit);
		assertEquals(visit,pet.getVisit("visit test"));
		assertEquals(visit,pet.getVisit("Visit Test"));
    }
    
    @Test
    @Transactional
    void testHasOperation() throws Exception {
    	Pet pet = new Pet();
    	Operation operation = new Operation();
    	operation.setDescription("test operation");
    	assertNull(pet.getOperation("test operation"));
    	assertNull(pet.getOperation("Test Operation"));
    	pet.addOperation(operation);
    	assertEquals(operation,pet.getOperation("test"));
    	assertEquals(operation,pet.getOperation("Test"));
    	  	
    	
    }
}
