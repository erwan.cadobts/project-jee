package org.springframework.samples.petclinic.model;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

import java.util.ArrayList;

import javax.transaction.Transactional;

import org.junit.jupiter.api.Test;

public class VetTest {
	
	@Test
	@Transactional
    void testHasMemo() throws Exception {
    	Vet vet = new Vet();
		Memo memo = new Memo();
		memo.setDescription("test");
		assertNull(vet.getMemo("test"));
		assertNull(vet.getMemo("Test"));
		vet.addMemo(memo);
		assertEquals(memo, vet.getMemo("test"));
		assertEquals(memo, vet.getMemo("Test"));
		
    }
	
    @Test
    @Transactional
    void testHasOperation() throws Exception {
    	Vet vet = new Vet();
    	Operation operation = new Operation();
    	operation.setDescription("operation test");
    	assertNull(vet.getOperation("operation test"));
    	assertNull(vet.getOperation("Operation Test"));
    	vet.addOperation(operation);
    	assertEquals(operation, vet.getOperation("operation test"));
    	assertEquals(operation, vet.getOperation("Operation Test"));
    	
    }
}
