package org.springframework.samples.petclinic.repository;

import java.util.List;

import org.springframework.dao.DataAccessException;
import org.springframework.samples.petclinic.model.BaseEntity;
import org.springframework.samples.petclinic.model.Operation;

public interface OperationRepository {
	
	/**
     * Retrieve an <code>Operation</code> from the data store by id.
     *
     * @param id the id to search for
     * @return the <code>Operation</code> if found
     * @throws org.springframework.dao.DataRetrievalFailureException if not found
     */
    Operation findById(int id);
    
    /**
     * Save a <code>Operation</code> to the data store, either inserting or updating it.
     *
     * @param memo the <code>Operation</code> to save
     * @see BaseEntity#isNew
     */
    void save(Operation operation) throws DataAccessException;

    List<Operation> findByVetId(Integer vetId);
    
    List<Operation> findByPetId(Integer petId);

}
