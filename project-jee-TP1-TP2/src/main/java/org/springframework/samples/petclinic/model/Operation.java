package org.springframework.samples.petclinic.model;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;

import org.springframework.beans.support.MutableSortDefinition;
import org.springframework.beans.support.PropertyComparator;
import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Table(name = "operations")
public class Operation extends BaseEntity{
	
	  @Column(name = "description")
	  @NotEmpty
	  private String description;
	  
	  @Column(name = "date")
	  @DateTimeFormat(pattern = "yyyy/MM/dd")
	  @NotEmpty
	  private LocalDate date;
	  
	  @ManyToOne
	  @NotEmpty
	  @JoinColumn(name = "vet_id")
	  private Vet vet;
	  
	  @ManyToOne
	  @NotEmpty
	  @JoinColumn(name = "pet_id")
	  private Pet pet;

	    public Operation() {
	        this.date = LocalDate.now();
	    }
	  
	    /**
	     * Getter for property date.
	     *
	     * @return Value of property date.
	     */
	    public LocalDate getDate() {
	        return this.date;
	    }

	    /**
	     * Setter for property date.
	     *
	     * @param date New value of property date.
	     */
	    public void setDate(LocalDate date) {
	        this.date = date;
	    }

	    /**
	     * Getter for property description.
	     *
	     * @return Value of property description.
	     */
	    public String getDescription() {
	        return this.description;
	    }
	    /**
	     * Setter for property description.
	     *
	     * @param description New value of property description.
	     */
	    public void setDescription(String description) {
	        this.description = description;
	    }

	    /**
	     * Getter for property vet.
	     *
	     * @return Value of property vet.
	     */
	    public Vet getVet() {
	        return this.vet;
	    }

	    /**
	     * Setter for property vet.
	     *
	     * @param vet New value of property vet.
	     */
	    public void setVet(Vet vet) {
	        this.vet = vet;
	    }
	    
	    /**
	     * Getter for property pet.
	     *
	     * @return Value of property pet.
	     */
	    public Pet getPet() {
	        return this.pet;
	    }

	    /**
	     * Setter for property pet.
	     *
	     * @param vet New value of property pet.
	     */
	    public void setPet(Pet pet) {
	        this.pet = pet;
	    }
}
