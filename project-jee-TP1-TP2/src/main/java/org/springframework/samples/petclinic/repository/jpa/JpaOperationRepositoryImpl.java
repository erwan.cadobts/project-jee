package org.springframework.samples.petclinic.repository.jpa;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.dao.DataAccessException;
import org.springframework.samples.petclinic.model.Operation;
import org.springframework.samples.petclinic.model.Owner;
import org.springframework.samples.petclinic.model.Visit;
import org.springframework.samples.petclinic.repository.OperationRepository;

public class JpaOperationRepositoryImpl implements OperationRepository {

	@PersistenceContext
    private EntityManager em;
	
	 
	@Override
	public Operation findById(int id) {
		Query query = this.em.createQuery("SELECT operation FROM Operation operation  WHERE operation.id =:id");
        query.setParameter("id", id);
        return (Operation) query.getSingleResult();
	}

	@Override
	public void save(Operation operation) throws DataAccessException {
        if (operation.getId() == null) {
            this.em.persist(operation);
        } else {
            this.em.merge(operation);
        }
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<Operation> findByVetId(Integer vetId) {
		Query query = this.em.createQuery("SELECT v FROM Operation v where v.vet.id= :id");
        query.setParameter("id", vetId);
        return query.getResultList();
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<Operation> findByPetId(Integer petId) {
		Query query = this.em.createQuery("SELECT v FROM Operation v where v.pet.id= :id");
        query.setParameter("id", petId);
        return query.getResultList();
	}

}
